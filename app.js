const btn = document.querySelector('.button')

btn.addEventListener('click', () => {
    const them = localStorage.getItem('bg') === 'true';
    localStorage.setItem('bg', !them);
    document.body.classList.toggle('background', !them);
});

document.addEventListener('DOMContentLoaded', () => {
    document.body.classList.toggle('background', localStorage.getItem('bg') === 'true');
});

